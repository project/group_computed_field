# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.0.0](https://git.drupal.org/project/group_computed_field/compare/2.0.0...3.0.0) (2023-10-24)


### ⚠ BREAKING CHANGES

* Sites using Drupal 9 and/or Group 2 will need to upgrade before upgrading this
module

### Build System

* remove drupal 9 and group 2 support ([8edad68](https://git.drupal.org/project/group_computed_field/commit/8edad6816d6e2142f16ac1675087fa79d4a7fb90))

## [2.0.0](https://git.drupal.org/project/group_computed_field/compare/1.0.1...2.0.0) (2023-10-24)


### ⚠ BREAKING CHANGES

* This also removes Drupal 8 support

### Features

* add d10 support ([3739d31](https://git.drupal.org/project/group_computed_field/commit/3739d31f019071e0bae76d91075b30261f970329))
* update install and uninstall hooks ([b6f038b](https://git.drupal.org/project/group_computed_field/commit/b6f038bf7f4e39e507c0f5b45ca52d422c162965))


### Bug Fixes

* allow related groups to be loaded ([20abfa3](https://git.drupal.org/project/group_computed_field/commit/20abfa3becf5d800e1da76850a12a366a3d9e20f))
* do not use interface ([d148002](https://git.drupal.org/project/group_computed_field/commit/d148002a4ced9cda02213a1ec363118a09379573))
* remove field from config on uninstall ([78ae8cb](https://git.drupal.org/project/group_computed_field/commit/78ae8cb48e1143dbf69787500a5c3117cdf7ef13))
* update module to not immediately break with groups 2 ([567d26f](https://git.drupal.org/project/group_computed_field/commit/567d26f364c110e16d25e3bb1e7ea7d2eed68584))
* use proper field plugin ([fe4ded4](https://git.drupal.org/project/group_computed_field/commit/fe4ded4865a7235617a504d419a01ea3c723f9f1))


### Performance Improvements

* clean up helper service ([4969b6a](https://git.drupal.org/project/group_computed_field/commit/4969b6ae0427252b0e4de0d61a738c379b5f05dc))


### Code Refactoring

* **group:** start port to group 2.x+ ([9a41f57](https://git.drupal.org/project/group_computed_field/commit/9a41f578548b6c68d4000b92750bd9a441145dc5))

### [1.0.1](https://git.drupal.org/project/group_computed_field/compare/1.0.0...1.0.1) (2022-01-11)


### Bug Fixes

* **deprecations:** update services ([3ebd9ca](https://git.drupal.org/project/group_computed_field/commit/3ebd9ca967ef414193884097e9e01c8820781363))

## [1.0.0](https://git.drupal.org///compare/1.0.0-alpha1...1.0.0) (2021-05-06)


### Build System

* clean up config files ([b65055a](https://git.drupal.org///commit/b65055a42607d0063b712dc216d03ea956560a81))


### Documentation

* add installation instructions for composer ([55cdda3](https://git.drupal.org///commit/55cdda3f480b506d78065d994bcb79d927e2d714))

## [1.0.0-alpha1](https://git.drupal.org///compare/1.0.0-alpha0...1.0.0-alpha1) (2021-04-19)


### Bug Fixes

* **field:** use correct entity reference field classes ([83cb75e](https://git.drupal.org///commit/83cb75e9b990a10ead1bffecb27614b67668693e))

## [1.0.0-alpha0](https://git.drupal.org///compare/v0.0.0...v1.0.0-alpha0) (2021-03-11)


### Features

* **build:** add gitattributes ([24c2f29](https://git.drupal.org///commit/24c2f295dc5905e6a38ca033d498550e5e2406ec))
* add initial functionality ([5361986](https://git.drupal.org///commit/53619860566512aa28126edab19687309ab35ecc))


### Bug Fixes

* **helper:** check if entity is new before loading groups ([b35b6a9](https://git.drupal.org///commit/b35b6a9767e283a6f6245e0605bf74d8f5042454))
* correct class naming ([802cf48](https://git.drupal.org///commit/802cf48ae1d2cd11529bf9811850d797be93ae0b))
* ensure entities definitions are updated on install/uninstall ([d42fb6e](https://git.drupal.org///commit/d42fb6eebcfa58a781d0efef293a49b7dc78af9e))
* remove version handling for composer and info files ([1c23599](https://git.drupal.org///commit/1c2359904a325f6504eef4c3b945f50b700af719))


### Code Refactoring

* **naming:** use proper names for entity identification ([8b1950a](https://git.drupal.org///commit/8b1950abc2babd9a5076ba75843d2a1d60755470))


### Build System

* **version:** remove v from auto tagging ([260c150](https://git.drupal.org///commit/260c150277e6606612a9e1f10939ea31d4f693d6))


### Continuous Integration

* remove unused templates ([f1dfba2](https://git.drupal.org///commit/f1dfba23e1c4ccf5756ec7944bf778b78f3e47b6))


### Documentation

* **readme:** update description ([6acc4f7](https://git.drupal.org///commit/6acc4f7bf588e025761959c2763e33024c647b0c))
* **readme:** update information to be accurate ([6b2844f](https://git.drupal.org///commit/6b2844f28fb1628e15a467549dbbeb8e0ab6860c))
