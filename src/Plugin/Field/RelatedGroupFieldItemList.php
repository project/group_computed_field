<?php

namespace Drupal\group_computed_field\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\group\Entity\GroupRelationship;

/**
 * The "Related Groups" field item list class.
 */
class RelatedGroupFieldItemList extends FieldItemList implements EntityReferenceFieldItemListInterface {

  use ComputedItemListTrait;

  /**
   * {@inheritDoc}
   */
  protected function computeValue() {
    $related_groups = $this->getRelatedGroups();

    foreach ($related_groups as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function referencedEntities() {
    $related_groups = $this->getRelatedGroups();

    return $related_groups;
  }

  /**
   * Get the related groups for the item list's entity.
   *
   * @return array
   *   The array of related groups.
   */
  protected function getRelatedGroups() {
    $related_groups = [];
    $group_relationship_entities = GroupRelationship::loadByEntity($this->getEntity());

    foreach ($group_relationship_entities as $group_relationship_entity) {
      $group = $group_relationship_entity->getGroup();
      $related_groups[] = $group;
    }

    return $related_groups;
  }

}
